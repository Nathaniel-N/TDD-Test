// ChatBox

$("#message").on("keyup", function(e){
    if (e.which == 13){
        var text = $(this).val();
        if (text !== "\n"){
            var x = Math.floor(Math.random() * 10);
            var type = '';
            if(x<5){
                type = 'msg-send';
            }
            else{
                type = 'msg-receive';
            }
            $('<p>'+$(this).val()+'</p>').addClass(type).appendTo('.msg-insert');
            $(this).val('');
        }
        else{
            $(this).val('');
        }
    }
});

// END

// Calculator
var print = document.getElementById('print');
print.value = " ";
var erase = false;
var useLog = false;
var useSin = false;
var useTan = false;
var mayLogSinTan = true;

var go = function(x) {
    
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = " ";
    erase = false;
    mayLogSinTan = true;
  }
  else if (x === 'eval') {
      var output = ""
      
      if(useLog){
          output = Math.log(print.value.substring(4,))
      }
        else if(useSin){
          output = Math.sin(print.value.substring(4,))
      }
        else if(useTan){
          output = Math.tan(print.value.substring(4,))
      }
        else{
            output = Math.round(evil(print.value) * 10000) / 10000;
        }
      print.value = output
      erase = true;
      useLog = false;
      useSin = false;
      useTan = false;
      mayLogSinTan = true;
  }
  else if (x === 'log') {
        if(mayLogSinTan){
            if (erase) {
                print.value = " ";
                erase = false;
            }
            print.value += x;
            useLog = true;
            mayLogSinTan = false;
        }
      
  }
    else if (x === 'sin' ) {
      if(mayLogSinTan){
          if (erase) {
                print.value = " ";
                erase = false;
            }
            print.value += x;
            useSin = true;
            mayLogSinTan = false;
        }
  }
    else if (x === 'tan' ) {
      if(mayLogSinTan){
          if (erase) {
                print.value = " ";
                erase = false;
            }
            print.value += x;
            useTan = true;
            mayLogSinTan = false;
        }
  } else {
      if ( "0123456789".includes(x) && erase ){
          print.value = " ";
      }
        if ( " + - * / . ".includes(x) &&  !"0123456789".includes(print.value.slice(-1)) ){
          return;
      }
        if(" + - * / ".includes(x) && (useLog||useSin||useTan)) {
            return;
        }   
             
        erase = false;
        mayLogSinTan = false;
      print.value += x;
  }
  $('#print').val(print.value).html();
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

function changeTheme(theme) {
  $('body').css('background-color',theme.bcgColor);
  $('body').css('color',theme.bgcColor);
}


// Store
localStorage.setItem("themes", JSON.stringify([
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
])
);

var daftarTheme = {
  0 : {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  1 : {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  2 : {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  3 : {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  4 : {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  5 : {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  6 : {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  7 : {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  8 : {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  9 : {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  10 : {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"},
}
var themeNow = {"Indigo": {"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
if (!localStorage.getItem("selectedTheme")) {
  localStorage.setItem("selectedTheme", JSON.stringify(themeNow['Indigo']))
}
changeTheme(JSON.parse(localStorage.getItem("selectedTheme")))

$(document).ready(function() {
    $('.my-select').select2({
      'data': JSON.parse(localStorage.getItem("themes"))
    });
});

$('.apply-button').on('click', function() {  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var id = $('.my-select').val()
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    var objTheme = daftarTheme[id];
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    changeTheme(objTheme);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem("selectedTheme", JSON.stringify(objTheme));
});