

window.fbAsyncInit = function() {
    FB.init({
      appId      : '178002409448440',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
    FB.getLoginStatus(function(response){
      document.getElementById('status').innerHTML = 'hehe'
      if (response.status === 'connected') {
        console.log('connected');
        render(true);
      }
      else {
        console.log('not connected');
      }
   
    });
  };

  
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('#lab8').html(
        '<div class="profile">' +
          '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
          '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
          '<div class="data">' +
            '<h1>' + user.name + '</h1>' +
            '<h2>' + user.about + '</h2>' +
            '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
          '</div>' +
        '</div>'
        );
      $('#feedContainer').html (
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
        '<button class="logout" onclick="facebookLogout()">Logout</button>'
      );

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          console.log(value.id)
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#feedContainer').append(
              '<div class="feed">' +
                '<div>' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>' +
                '<button class="deleteButton" onclick="deleteStatus()">Delete</button>' +
              '</div>'
            );
          } else if (value.message) {
            $('#feedContainer').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
                '<button class="deleteButton" onclick="deleteStatus()">Delete</button>' +
              '</div>'
            );
          } else if (value.story) {
            $('#feedContainer').append(
              '<div class="feed">' +
                '<h2>' + value.story + '</h2>' +
                '<button class="deleteButton" onclick="deleteStatus()">Delete</button>' +
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
  }
};

const facebookLogin = () => {
  FB.login(response => {
    console.log("aaaa");
    if(response.status=='connected'){
      $(".button").hide();
      render(true);
    }
    else{
      render(false);
    }
  }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})
}

const facebookLogout = () => {
  FB.getLoginStatus(response => {
    if (response.status === 'connected') {
      FB.logout(response => {
        window.location.reload();
      });
    }
  });
}

const getUserData = (fun) => {
  FB.api('/me?fields=id,name,cover,gender,about,email,picture', 'GET', response => {
    fun(response)
  });
}

const getUserFeed = (fun) => {
  FB.api('/me/feed', 'GET', response => {
    fun(response);
  })
};

const postFeed = message => {
  FB.api('/me/feed', 'POST', {message:message}, response => {
  });
};


const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
};
postFeed(message);
};
