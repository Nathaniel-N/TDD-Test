from django.conf.urls import url
from .views import index
#from .views import LandingPageRedirectView
#from django.views.generic.base import RedirectView

    #url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    #url(r'^$', RedirectView.as_view(permanent=True, url='/lab-2/'))
]

# urlpatterns = [
#     url(r'^$', LandingPageRedirectView, name='LandingPageRedirectView'),
#     url(r'^$', RedirectView.as_view(permanent=True, url='/lab-2/'))
# ]
