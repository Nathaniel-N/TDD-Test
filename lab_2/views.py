from django.shortcuts import render
from lab_1.views import mhs_name, birth_date
#from django.views.generic.base import RedirectView


#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'

def index(request):    
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)

# class LandingPageRedirectView(RedirectView):
#     permanent = True
#     query_string = True
#     pattern_name = "landing-page" 
#     def get_redirect_url(self, *args, **kwargs):        
#         response = {'name': mhs_name, 'content': landing_page_content}
#         return render(request, 'index_lab2.html', response)
#         return super(LandingPageRedirectView, self).get_redirect_url(*args, **kwargs)